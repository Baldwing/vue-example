import Vue from 'vue';

new Vue({
    el: '#app',
    data() {
        return {
            output: 1,
        };
    },
    methods: {
        click() {
            this.output += 1;
        },
    },
    template: `
        <div>
            {{ output }}
            <button @click="click">
                Click me!
            </button>
        </div>
    `,
})